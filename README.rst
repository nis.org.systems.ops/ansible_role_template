Role Name
=========

Installs and configures ansible_role_template on RedHat/AlmaLinux.

Requirements
------------

- Ansible 2.9.22+

Pipeline
--------

The pipeline for this role is centralized, as seen in .gitlab-ci.yml:
  ---
  include:
    remote: 'https://code.vt.edu/ansible-roles/molecule/-/raw/master/.gitlab-ci.yml'

If a custom pipeline is desired, edit .gitlab-ci.yml, but be warned that future updates to
centralized pipeline repository will not be automatically included in this role if modified.

The pipeline directory allows for customization of the variables and tests for the
pipeline that are specific to this role. See the README in the pipeline directory in this project
for additional details.

Role Variables
--------------

The example variable

    ansible_role_template_variable

Dependencies
------------

None

Example Playbook
----------------

The following example shows how this role can be defined in a playbook with parameters passed to override default variables.

.. code-block:: yaml

    - hosts: servers
      roles:
        - role: ansible_role_template
          ansible_role_template_variable: 'example variable entry'
          tags: ansible_role_template
