Pipeline folder
------------

This folder is used to store the pipeline related files for setting variables and tests that
are specific to this role. The files in this role are as follows:

vars.yml

  This file is used to set variables to be used during the pipeline run of the playbook test. If this
  file is not found, no custom variables will be set during the pipeline run. The default can be found at:
  https://code.vt.edu/ansible-roles/molecule-docker/-/blob/master/default/vars.yml

test_default.py

  This file is used for specifying the desired testinfra tests to be run. If this file is not found,
  the test will be the standard check for /etc/hosts on the system. The default can be found at:
  https://code.vt.edu/ansible-roles/molecule-docker/-/raw/master/resources/tests/test_default.py
